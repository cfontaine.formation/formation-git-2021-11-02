

 Créer un merge request

 Récupérer la branche distante

 Créer un branche locale à partir de branche distante

 Créer un fichier Adresse.java


public class Adresse {
    private long id;
    private String rue;
    private String ville;
    private String codePostal
    
    public Adresse(String rue, String ville, String codePostal) {
        super();
        this.rue = rue;
        this.ville = ville;
        this.codePostal = codePostal;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    @Override
    public String toString() {
        return "Adresse [id=" + id + ", rue=" + rue + ", ville=" + ville + ", codePostal=" + codePostal + "]";
    }
    
}