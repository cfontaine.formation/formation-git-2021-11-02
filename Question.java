public class Question {
    
    private long id;
    private String intitule;
    private List<Answer> answers;
    
    public Question(String intitule, List<Answer> answers) {
        super();
        this.intitule = intitule;
        this.answers = answers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "Question [id=" + id + ", intitule=" + intitule + "]";
    }
}
